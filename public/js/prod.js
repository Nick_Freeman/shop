let result = '';
$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('#csrf').val()
        }
    });

    $.ajax({
        url: $('#url').val(),
        method: "POST",
    }).done(function (mass) {


        for (let i = 0; i < mass.length; i++) {
            let image = '<img src="/uploads/' + mass[i].image + '" class="img-fluid" alt="Responsive image">'
            //
            result += '<div class="col-md-4 products">'
                + image +
                '<h2 class="name">' + mass[i].title + '</h2>' +
                '<h2>Type:' + mass[i].type + '</h2>' +
                '<h2 class="price" >' + mass[i].price + '</h2>' +
                '<p>Type:' + mass[i].short_description + '</p>' +
                '</div>';
            //
        }
        $('#tovar').append(result)
    });
});

function selectionChange() {
    let select = document.getElementById("sorting");
    let value = select.value;
    //get price
    let price = document.querySelectorAll('.price');
    let priceMass = [];
    let i = 0;
    price.forEach((item) => {
        priceMass[i] = item.innerHTML;
        i++
    })
    //
    //get name
    let name = document.querySelectorAll('.name');
    let nameMass = [];
    let k = 0;
    name.forEach((items) => {
        nameMass[k] = items.innerHTML;
        k++
    })
    //
    switch (value) {
        case "Sort_By_Price_Up" :
            priceMass.sort(function (a, b) {
                return a - b
            });
            console.log(priceMass);
            console.log("it's PRICE UP");
            break;
        case "Sort_By_Price_Down" :
            priceMass.sort(function (a, b) {
                return b - a
            });
            console.log(priceMass);
            console.log("it's PRICE DOWN");
            break;
        case "Sort_By_Alphabet" :
            nameMass.sort();
            console.log(nameMass);
            console.log("it's APHABET");
            break;
        case "Sort_By_Aphabet_Revers" :
            nameMass.sort();
            nameMass.reverse();
            console.log(nameMass);
            console.log("it's APHABET REVERS");
            break;
    }
}
