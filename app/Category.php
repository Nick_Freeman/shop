<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{

    protected $fillable = ['category_name','parent_id','level'];

    public function productCategory(){
        return $this->belongsToMany(CategoryProduct::class);
    }


}
