<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CommentVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::route()->getName()){
            case 'comment.update':
                return [
                    'id' => 'required|numeric|exists:posts,id',
                    'author' => 'required|max:20',
                    'body' => 'required|max:223',
                    'product_id' => 'exists:products,id',
                ];
                break;
            case 'comment.store':
                return [
                    'body' => 'required|max:223',
                    'author' => 'required|max:20',
                    'product_id' => 'exists:products,id',
                ];
                break;

        };
    }

    public function messages()
    {
        return [
            'author.max'  => 'This field is required, maximum length is 20 characters.',
            'body.max'  => 'This field is required, maximum length is 223 characters.',

        ];
    }
}
