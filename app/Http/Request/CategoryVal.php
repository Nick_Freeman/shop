<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class CategoryVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::route()->getName()){
            case 'category.update':
                return [
                    'id' => 'required|numeric|exists:posts,id',
                    'category_name' => 'required|max:20',
                    'parent_id' => 'required|max:223',
                    'level' => 'required',
                ];
                break;
            case 'category.store':
                return [
                    'category_name' => 'required|max:223',
                    'parent_id' => 'required|max:20',
                    'level' => 'required',
                ];
                break;

        };
    }

    public function messages()
    {
        return [
            'category_name'  => 'This field is required, maximum length is 20 characters.',


        ];
    }
}
