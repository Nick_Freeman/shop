<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;

class OrderVal extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch (Request::route()->getName()){
            case 'order.update':
                return [
                    'id' => 'required|numeric|exists:posts,id',
                    'name' => 'required|min:2|max:20',
                    'email' => 'required|email',
                    'phone' => 'required|min:5|phone',
                ];
                break;
            case 'order.store':
                return [
                    'name' => 'required|min:2|max:20',
                    'email' => 'required|email',
                    'phone' => 'required|min:5|phone'
                ];
                break;

        };
    }

    public function messages()
    {
        return [
            'name.max'  => 'This field is required, maximum length is 20 characters.',
            'email' => 'Please , enter valid email.'
        ];
    }
}
