<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\RegVal;
use Illuminate\Http\Request;

class RegistrationController extends Controller
{
//    public function create(){
//        return view('users.create');
//    }
//
//    public function store(RegVal $request){
//
//        $user = User::create([
//            'name' => request()->post('name'),
//            'email' => request()->post('email'),
//            'password' => bcrypt(request()->post('password')),
//        ]);
//
//        $user = new User();
//
//        $user->fill($request->only('name','email'));
//
//
//        $user->save();
//
//
//        auth()->login($user);
//
//        return redirect('/');
//    }

    public function create(){
        return view('users.create');
    }

    public function store(){
        $this->validate(request(), [
            'name' => 'required|min:3',
            'role' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed|min:6'
        ]);
        $user = User::create([
            'name' => request()->post('name'),
            'role' => request()->post('role'),
            'email' => request()->post('email'),
            'password' => bcrypt(request()->post('password')),
        ]);

        auth()->login($user);

        return redirect('/');
    }
}