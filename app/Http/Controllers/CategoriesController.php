<?php

namespace App\Http\Controllers;

use App\Product;
use App\Category;
use App\Http\Requests\CategoryVal;
use Couchbase\RegexpSearchQuery;
use Illuminate\Http\Request;

class CategoriesController extends BaseController
{
    public function __construct()
    {
        $this->model = 'App\Category';
        $this->rootView = 'categories';
        $this->outPutVariable = 'categories';
        $this->outPutVariableForSingle = 'category';
        $this->baseRouteName = 'category';
    }

//    public function show(Category $id){
//            dd($id);
//       // $category
//    }

    public function show($id){
        $categor = '';
        $typez = Product::find(type);
        $categor .= $typez;


        return $categor;
    }

    public function categoryTableToAjax()
    {
            $categories = Category::all()->toArray();
            foreach ($categories as $key => $category){
                $categories[$key]['url'] = route('category.show',['id' => $category['id']]);
            }
        return response($categories);
    }

    public function store(CategoryVal $request)
    {

        $category = new Category();

        $category->fill($request->only('category_name', 'parent_id', 'level'));

        $category->save();

        return json_encode([
            'category_name' => $category->category_name,
            'parent_id' => $category->parent_id,
            'level' => $category->level,
        ]);
    }
}
