<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderVal;
use App\Order;
use App\Cart;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    public function create(){
    $cart = Cart::getCart();
    if(count($cart) < 1){
        return redirect()->route('product.index');
    }
    $products = Cart::getCartProducts($cart);

    $products->save();
}

    public function store(OrderVal $request){

        $order = Order::create(request()->all());

        $cart = Cart::getCart();
        foreach ($cart as $product_id => $amount){
            $order->products()->attach($product_id, ['amount' => $amount]);
        }

            $order->save();

        return  json_encode([
            'name'=>$order->name,
            'email'=>$order->email,
            'phone'=>$order->phone,
            'amount'=>$order->amount,

        ]);
    }
}
