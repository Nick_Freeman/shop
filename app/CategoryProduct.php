<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryProduct extends Model
{
    protected $fillable = ['category_id','product_id'];

    public function product(){
        return $this->hasMany (Product::class);
    }
    public function Category(){
        return $this->hasMany(Category::class);
    }
}
