@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">{{$product->title}}</h1>
    <p>{{$product->short_description}}</p>
@endsection

@section('main_content')
    <div class="col-md-12">
        <form action="{{route('product.destroy', ["product" => $product->id])}}" method="post">
            {{method_field('delete')}}
            {{csrf_field()}}
            <h2>Are you sure you want to delete <b>{{$product->title}}</b> ?</h2>
            <div class="form-group">
                <button class="btn btn-danger">yes</button>
                <a class="btn btn-warning" href="{{route('product.index')}}">no</a>
            </div>
        </form>
    </div>
@endsection