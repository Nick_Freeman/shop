@extends('layouts/main')


@section('jumbotron')

    @if(Auth::check())
        @if(Auth::user()->role == 'admin')
            <div class="btn btn-success">
                <a class="btn btn-success" href="{{route('product.create')}}">CREATE NEW PRODUCT</a>
            </div>
        @endif
    @endif
    <h1 class="display-3">Products</h1>

@endsection
@section('leftmenu')
    <div id="category">
        <input name="cat" id="cat" type="hidden" value="{{route('category.ajax.table')}}">
    </div>

@endsection

@section('main_content')



    <select id="sorting" onchange="selectionChange()">
        <option value="Sort By">Sort Products</option>
        <option value="Sort_By_Price_Up" id="price_up">Sort By Price Up</option>
        <option value="Sort_By_Price_Down" id="price_down">Sort By Price Down</option>
        <option value="Sort_By_Alphabet" id="a_z">Sort By A-Z</option>
        <option value="Sort_By_Aphabet_Revers" id="z_a">Sort By Z-A</option>
    </select>

    <div id="main">
        <div id="tovar">
            <input type="hidden" id="csrf" value="{{csrf_token()}}"/>
            <input name="url" id="url" type="hidden" value="{{route('product.ajax.table')}}">
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('js/prod.js') }}"></script>

@endsection






