@extends('layouts.main')

@section('jumbotron')
    <h1 class="display-3">Edit Product:</h1>
    <p>Here we can edit product</p>
@endsection

@section('main_content')

    <form action="{{route('product.update',["id" => $product->id])}}" method="post" enctype="multipart/form-data">

        @include('layouts.embed.errors')

        {{csrf_field()}}

        <div class="form-group">
            <label for="title">Title:</label>
            <input class="form-control" value="{{$product->title}}" type="text" name="title" id="title">
        </div>

        <div class="form-group">
            <label for="slug">Slug:</label>
            <input class="form-control" value="{{$product->slug}}" type="text" name="slug" id="slug">
        </div>

        <div class="form-group">
            <label for="type">Type:</label>
            <input class="form-control" value="{{$product->type}}" type="text" name="type" id="type">
        </div>

        <div class="form-group">
            <label for="price">Price:</label>
            <input class="form-control" value="{{$product->price}}" type="text" name="price" id="price">
        </div>

        <div class="form-group">
            <label for="image">Upload image:</label>
            <input class="form-control" value="{{old('image')}}" type="file" name="image" id="image">
        </div>

        <div class="form-group">
            <label for="short_description">Short description:</label>
            <textarea class="form-control"  name="short_description" id="short_description">{{$product->short_description}}</textarea>
        </div>

        <div class="form-group">
            <label for="body">Body:</label>
            <textarea class="form-control" name="body" id="body">{{$product->body}}</textarea>
        </div>
            <input name="id" type="hidden" value="{{$product->id}}">
        <div class="form-group">
            <button class="btn btn-primary" type="submit">Update</button>
        </div>

    </form>
@endsection