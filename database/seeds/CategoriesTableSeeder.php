<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name' => 'homeElectronic',
                'parent_id' => '0',
                'level'  => '0'
            ],
            [
                'category_name' => 'games',
                'parent_id' => '0',
                'level'  => '0'
            ],
            [
                'category_name' => 'cars',
                'parent_id' => '0',
                'level'  => '0'
            ]
        ]);
    }
}
